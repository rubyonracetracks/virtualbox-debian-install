#!/bin/bash

DIR_PWD=$PWD

BRANCH=''
OS_RELEASE=`cat /etc/os-release`

grep_release () {
  STR=$1
  BR=$2
  IS_RELEASE=`cat /etc/os-release | grep "$STR"`
  if [[ "$IS_RELEASE" != '' ]]; then
    BRANCH="$BR"
  fi
}

# Automatically determine which branch of Debian is installed

# For SparkyLinux 4
grep_release 'tyche' 'stretch'
grep_release 'Tyche' 'stretch'

# For SparkyLinux 5
grep_release 'nibiru' 'buster'
grep_release 'Nibiru' 'buster'

# For Debian and MX Linux
grep_release 'stretch' 'stretch'
grep_release 'Stretch' 'stretch'
grep_release 'buster' 'buster'
grep_release 'Buster' 'buster'

# For Ubuntu Linux
grep_release 'trusty' 'trusty'
grep_release 'Trusty' 'trusty'
grep_release 'xenial' 'xenial'
grep_release 'Xenial' 'xenial'
grep_release 'bionic' 'bionic'
grep_release 'Bionic' 'bionic'

# What to do if the Debian/Ubuntu version cannot be determined
while [[ "$BRANCH" != 'stretch' && "$BRANCH" != 'buster' && "$BRANCH" != 'trusty' && "$BRANCH" != 'xenial' && "$BRANCH" != 'bionic' ]]; do
  echo 'Which branch of Debian/Ubuntu is installed on your system?'
  echo "Enter 'stretch' to select Debian Stretch."
  echo "Enter 'buster' to select Debian Buster."
  echo "Enter 'trusty' to select Ubuntu Trusty."
  echo "Enter 'xenial' to select Ubuntu Xenial."
  echo "Enter 'bionic' to select Ubuntu Bionic."
  read BRANCH
done

# Debian Buster uses the same VirtualBox pacage as Ubuntu 18.04 (Bionic Beaver)
if [[ "$BRANCH" == 'buster' ]]; then
  BRANCH="bionic"
fi

echo '-------------------'
echo 'sudo apt-get update'
sudo apt-get update

echo '----------------------------'
echo 'sudo apt-get install -y wget'
sudo apt-get install -y wget

echo '------------'
echo 'Getting keys'
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

echo '----------------------------------------------'
echo 'Adding /etc/apt/sources.list.d/virtualbox.list'

SOURCE_VIRTUALBOX="deb https://download.virtualbox.org/virtualbox/debian $BRANCH contrib"
echo $SOURCE_VIRTUALBOX | sudo tee /etc/apt/sources.list.d/virtualbox.list
sudo apt-get update

echo '***************************'
echo 'BEGIN installing VirtualBox'
echo '***************************'

# Install VirtualBox
sudo apt-get -y install virtualbox-6.0

wait

echo '--------------------'
echo 'VBoxManage --version'
VBoxManage --version

echo '******************************'
echo 'FINISHED installing VirtualBox'
echo '******************************'
