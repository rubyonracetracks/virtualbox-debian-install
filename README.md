# Installing VirtualBox in Debian or Ubuntu
This [Ruby on Racetracks](http://www.rubyonracetracks.com/) repository contains scripts for automatically installing [VirtualBox](https://www.virtualbox.org) in a Debian or Ubuntu host environment.

## Prerequisites
* If you are following the Ruby on Racetracks way, you MUST be using a 64-bit OS.  While VirtualBox still supports 32-bit systems, I no longer do.
* Debian Stretch, Debian Buster, or one of their derivatives should be installed on your computer as the host OS.  If you are using Windows, MacOS, or any other Linux distro, follow the instructions in the [VirtualBox](https://www.virtualbox.org) web site.
* Git should be installed in your Debian installation.  If you have not installed Git, you can install it by entering the following command in the terminal:
```
sudo apt-get update; sudo apt-get -y install git
```

## Procedure
* Enter the following commands in the terminal:

```
cd
mkdir -p rubyonracetracks
cd rubyonracetracks
git clone https://gitlab.com/rubyonracetracks/virtualbox-debian-install.git
cd virtualbox-debian-install
```
* Enter the command "bash main.sh" to install VirtualBox.  This will take a few minutes.
* Open the file manager.  Go to the rubyonracetracks directory you created in your user home directory.  Delete the virtualbox-debian-install directory, because you no longer need it.
* Congratulations!  You have successfully installed VirtualBox.
